-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2023 at 10:10 AM
-- Server version: 5.7.34
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamleshyadav_countdown`
--

-- --------------------------------------------------------

--
-- Table structure for table `timer`
--

CREATE TABLE `timer` (
  `id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `initial_value` text NOT NULL,
  `initial_timer_type` varchar(255) NOT NULL,
  `reset_time` text NOT NULL,
  `countdown_time` varchar(255) NOT NULL,
  `reset_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reset_type` text NOT NULL,
  `status` bigint(1) NOT NULL DEFAULT '0',
  `veryfy_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `timer`
--

INSERT INTO `timer` (`id`, `project_name`, `start_date`, `initial_value`, `initial_timer_type`, `reset_time`, `countdown_time`, `reset_update_time`, `reset_type`, `status`, `veryfy_id`) VALUES
(12, 'Test Parvindar', '2023-05-03 17:10:00', '2', 'hour', '1', '2023-05-03 17:13:59', '2023-05-04 13:22:14', 'hour', 0, ''),
(13, 'testing', '2023-05-03 17:21:00', '12', 'hour', '12', '2023-05-04 22:56:13', '2023-05-04 13:22:19', 'hour', 0, ''),
(19, 'plrfunnel', '2023-05-12 14:28:00', '90', 'minute', '30', '2023-05-12 15:58:00', '2023-05-04 13:28:21', 'minute', 0, ''),
(20, 'Ravi test', '2023-06-04 00:32:00', '1', 'minute', '1', '2023-06-04 1:32:00', '2023-05-04 13:41:34', 'hour', 1, ''),
(21, 'Jocelyn Gallagher', '1983-11-18 16:59:00', '1', 'minute', '10', '1983-11-18 17:59:00', '2023-05-04 13:22:26', 'minute', 0, ''),
(22, 'Test Parvindar', '2023-05-04 17:12:00', '1', 'minute', '2', '2023-05-04 19:00:41', '2023-05-04 13:30:17', 'minute', 0, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `timer`
--
ALTER TABLE `timer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `timer`
--
ALTER TABLE `timer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
