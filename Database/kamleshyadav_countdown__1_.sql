-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 05, 2023 at 03:44 PM
-- Server version: 5.7.34
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kamleshyadav_countdown`
--

-- --------------------------------------------------------

--
-- Table structure for table `timer`
--

CREATE TABLE `timer` (
  `id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `start_date` datetime NOT NULL,
  `initial_value` text NOT NULL,
  `initial_timer_type` varchar(255) NOT NULL,
  `reset_time` text NOT NULL,
  `countdown_time` varchar(255) NOT NULL,
  `reset_update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `reset_type` text NOT NULL,
  `status` bigint(1) NOT NULL DEFAULT '0',
  `veryfy_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `timer`
--

INSERT INTO `timer` (`id`, `project_name`, `start_date`, `initial_value`, `initial_timer_type`, `reset_time`, `countdown_time`, `reset_update_time`, `reset_type`, `status`, `veryfy_id`) VALUES
(22, 'Test Parvindar', '2023-05-06 01:21:00', '1', 'minute', '2', '2023-05-06 1:22:00', '2023-05-05 10:10:09', 'hours', 1, 'ecd8c96l'),
(24, 'ravi Testing', '2023-05-05 20:30:00', '2', 'hours', '90', '2023-05-05 22:30:00', '2023-05-05 09:10:40', 'minute', 1, '0aded9b3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `timer`
--
ALTER TABLE `timer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `timer`
--
ALTER TABLE `timer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
