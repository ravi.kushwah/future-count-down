<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Methods: GET, OPTIONS, POST");
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function index(){
        echo "No direct script access allowed";
    }
	function live($vid="")	{
		date_default_timezone_set('Asia/Kolkata');
		$data['d'] = $this->db_model->select_data('*','timer',array('status !='=>0,'veryfy_id'=>$vid));
		$start = isset($data['d'][0]['start_date'])?$data['d'][0]['start_date']:'';	
		$countdown_time = isset($data['d'][0]['countdown_time'])?$data['d'][0]['countdown_time']:'';
		
		if(empty($data['d'])){
		    $data['featuredate'] = "";
		}else{
		    $data['featuredate'] =date('F d Y h:i:s A',strtotime('+0 hour',strtotime($countdown_time)));
		}
	
		$data['startdata'] = $start;
		$this->load->view('admin/index',$data);
	}
	function updateCountDown(){
		date_default_timezone_set('Asia/Kolkata');
		$c_deatils = $this->db_model->select_data('*','timer',array('status !='=>0));
		$cdcheck = date('Y-m-d G:i:s', time());
	    $rut = $c_deatils[0]['reset_update_time'];
		$cd = date('Y-m-d h:i:s a', time());
		if($rut>$cd){
	    	$res = array('status'=>0);
		}else{
    		$fcd = date('Y-m-d G:i:s',strtotime('+'.$c_deatils[0]['reset_time'].' '.$c_deatils[0]['reset_type'].'',strtotime($cd)));
    	
    		$data = array(
    			'countdown_time'=>$fcd
    		);
    		$r = $this->db_model->update_data('timer',$data,array('status'=>1));
    		$res = array('status'=>1);
		}
		echo json_encode($res);
	}
	function check(){
		$d = $this->db_model->select_data('*','timer',array('status !='=>0,'veryfy_id'=>$_POST['vryfy']));
	    if(!empty($d)){
    		$countdown_time = $d[0]['countdown_time'];		
    		$featuredate = date('F d Y h:i:s',strtotime('+0 hour',strtotime($countdown_time)));
    	    $res = array('checkTime'=>$featuredate,'status'=>1,'countdown_time'=>$countdown_time);
	    }else{
	       $res = array('checkTime'=>"",'status'=>0,'countdown_time'=>""); 
	    }
	     echo json_encode($res);
		die;
	}
	function timerView(){
		$this->load->view('admin/addTimer');
	}
	function ViewDetails(){
		$this->load->view('admin/details');
	}
	function addTImer(){
	    $c_deatils = $this->db_model->select_data('*','timer',array('status !='=>0));
	   
	    if($_POST['hours']!='true'){
	        $time = "minute";
            $initialvalue = $_POST['minute_value'];
	    }else{
            $time = "hours";
	        $initialvalue = $_POST['minute_hours'];
	    }
	    if($_POST['resthours']!='true'){
	        $rtime = "minute";
	        $resetvalue = $_POST['rest_times'];
	    }else{
            $rtime = "hours";
            $resetvalue = $_POST['rest_hours'];
	    }
	    if($c_deatils){
	       $s = 0;
	    }else{
	       $s = 1;
	    }
	    $vryfyId = substr( md5(time().uniqid()), 1, 8 );
	    $countdown_time = date('Y-m-d G:i:s',strtotime('+'.$initialvalue.' '.$time.'',strtotime($_POST['datetime'])));
	    $start_date = date('Y-m-d G:i:s',strtotime('+ 0 hour',strtotime($_POST['datetime'])));
   		$data = array(
			'project_name'=>$_POST['project_name'],
			'start_date'=>$start_date,
			'initial_value'=>$initialvalue,
			'initial_timer_type'=>$time,
			'countdown_time'=>$countdown_time,
			'reset_time'=>$resetvalue,
			'reset_type'=>$rtime,
			'status'=>$s,
			'veryfy_id'=>$vryfyId
		);
		$res = $this->db_model->insert_data('timer',$data);
		if($res){
		    $ms = array('status'=>1,'res'=>'Successfully add Countdown time.');
		}else{
		     $ms = array('status'=>0,'res'=>'Technical issue ');
		}
		echo json_encode($ms);
	}
	function  EditTimerView($id){
	    $data['c_deatils'] = $this->db_model->select_data('*','timer',array('id'=>$id));
    	$this->load->view('admin/addTimer',$data);
	}
    function timeedit(){
        $id = $_POST['id'];
        $c_deatils = $this->db_model->select_data('*','timer',array('id'=>$id));
	   
	    if($_POST['hours']!='true'){
	        $time = "minute";
            $initialvalue = $_POST['minute_value'];
	    }else{
            $time = "hours";
	        $initialvalue = $_POST['minute_hours'];
	    }
	    if($_POST['resthours']!='true'){
	        $rtime = "minute";
	        $resetvalue = $_POST['rest_times'];
	    }else{
            $rtime = "hours";
            $resetvalue = $_POST['rest_hours'];
	    }
	    
	    $countdown_time = date('Y-m-d G:i:s',strtotime('+'.$initialvalue.' '.$time.'',strtotime($_POST['datetime'])));
	    $start_date = date('Y-m-d G:i:s',strtotime('+ 0 hour',strtotime($_POST['datetime'])));
   		$data = array(
			'project_name'=>$_POST['project_name'],
			'start_date'=>$start_date,
			'initial_value'=>$initialvalue,
			'countdown_time'=>$countdown_time,
			'reset_time'=>$resetvalue,
			'reset_type'=>$rtime,
		);
		$res = $this->db_model->update_data('timer',$data,array('id'=>$id));
		if($res){
		    $ms = array('status'=>1,'res'=>'Successfully Update Countdown time.');
		}else{
		     $ms = array('status'=>0,'res'=>'Technical issue ');
		}
		echo json_encode($ms);
    }
	function details(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
            $post = $this->input->post(NULL,TRUE);
            $get = $this->input->get(NULL,TRUE);
            if(isset($post['length']) && $post['length']>0){
                if(isset($post['start']) && !empty($post['start'])){
                    $limit = array($post['length'],$post['start']);
                    $count = $post['start']+1;
                }else{ 
                    $limit = array($post['length'],0);
                    $count = 1;
                }
            }else{
                $limit = '';
                $count = 1;
            }
        
            if($post['search']['value'] != ''){
                $like = array('project_name',$post['search']['value']);
            }else{
               $like = ''; 
            }
            $timer = $this->db_model->select_data('*','timer','',$limit,array('id','desc'),$like);
    
            if(!empty($timer)){
               
                foreach($timer as $key=>$v){
                   
                    if($v['status'] == 1){
                        $statusDrop = '<div class="admin_tbl_status_wrap"><button type="button" class="btn btn-success btn-xs dt-edit changeStatusButton" data-id="'.$v['id'].'" data-table ="timer" data-status ="0" style="margin-right:16px;">Active</button></div>';
                    }else{
                        $statusDrop = '<div class="admin_tbl_status_wrap"><button type="button" class="btn btn-danger btn-xs dt-edit changeStatusButton" data-id="'.$v['id'].'" data-table ="timer" data-status ="1" style="margin-right:16px;">Inactive</button></div>';
                    }
                     
                  $js = 'let vryfy = "'.$v['veryfy_id'].'"; let stDate = "'.$v['start_date'].'";let base_url = "'.base_url().'";const days=document.querySelector("#days");const hours=document.querySelector("#hours");const minutes=document.querySelector("#minutes");const seconds=document.querySelector("#seconds");const currentYear=new Date().getFullYear();const newYear=new Date("'.$v['countdown_time'].'");$.ajax({method: "POST",url: "'.base_url().'Welcome/check",	data:{vryfy:vryfy},success: function(resp) {var data = JSON.parse(resp);const currentDate=new Date();const newYear1=new Date(data.countdown_time);console.log(data.checkTime);if(data.status==0){return false;}else{if(data.checkTime<currentDate){update();}else{setInterval(function(){const currentDate=new Date();const diff=newYear1-currentDate;if(diff >= "0"){function UpdateTime(){const currentDate=new Date();const diff=newYear1-currentDate;const d=Math.floor(diff/1000/60/60/24);const h=Math.floor((diff/1000/60/60)%24);const m=Math.floor((diff/1000/60)%60);const s=Math.floor((diff/1000)%60);const ms=Math.floor(diff%1000);days.innerHTML=d<10?"0"+d:d;hours.innerHTML=h<10?"0"+h:h;minutes.innerHTML=m<10?"0"+m:m;seconds.innerHTML=s<10?"0"+s:s;}setInterval(UpdateTime,1000);}else{clearInterval();update();} } } });function update(){var i = 1;if(i==1){xhrPool = [];$.ajax({method: "POST",url: "'.base_url().'Welcome/updateCountDown",success: function(jqXHR,resp) {  xhrPool.push(jqXHR); window.location.reload();clearInterval();}});return "update Time";i++;}else{clearInterval();return window.location.reload();xhrPool.push(jqXHR);}}';
                  $html = '<div class="box"><h2 id="days">00</h2><small>Days</small></div><div class="box"><h2 id="hours">00</h2><small>Hours</small></div><div class="box"><h2 id="minutes">00</h2><small>Minutes</small></div><div class="box"><h2 id="seconds">00</h2><small>Seconds</small></div>   ';
                  $action = '<button type="button" class="btn btn-primary btn-xs copyButton" data-get-id="copyText1'.$key.'"  style="margin-right:16px;" title="Copy Js"><i class="fa fa-copy"></i></button>
                             <span id="copyText1'.$key.'" hidden> '.$js.'</span>
				             <button type="button" class="btn btn-primary btn-xs copyButtonhtml" onclick="withoutJquery();" style="margin-right:16px;" title="Copy HTML"><i class="fa fa-code"></i></button>
                             <span id="copyText2" hidden> '.$html.'</span>
                             <button type="button" class="btn btn-success btn-xs  editTImer" data-id="'.$v['id'].'" data-table ="timer" title="Delete"><i class="	fa fa-edit "></i></button>
                             <button type="button" class="btn btn-danger btn-xs dt-delete deleteData" data-id="'.$v['id'].'" data-table ="timer" title="Delete"><i class="fa fa-trash "></i></button>
                             <button type="button" class="btn btn-danger btn-xs" data-id="'.$v['veryfy_id'].'" data-table ="timer" title="Live Priview"><a target="_blank" href="'.base_url().'live/'.$v['veryfy_id'].'"><i class="fa fa-eye "></i></a></button>';
                      $dataarray[] = array(
                        $v['project_name'],
                        $v['start_date'],
                        $v['initial_value'],
                        $v['reset_time'],
                        $v['countdown_time'],
                        $statusDrop,
                        $action   
                    ); 
                    $count++;
                }
    
                $recordsTotal = $this->db_model->countAll('timer','','','',$like);
    
                $output = array(
                    "draw" => $post['draw'],
                    "recordsTotal" => $recordsTotal,
                    "recordsFiltered" => $recordsTotal,
                    "data" => $dataarray,
                );
    
            }else{
                $output = array(
                    "draw" => $post['draw'],
                    "recordsTotal" => 0,
                    "recordsFiltered" => 0,
                    "data" => array(),
                );
            }
            echo json_encode($output,JSON_UNESCAPED_SLASHES);
        }else{
            echo "data not found";
        } 
    } 
     function change_status(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
            if(!empty($this->input->post('id',TRUE))){
                // $this->db_model->update_Data_limit($this->input->post('table',TRUE),array('status'=>0),array('status'=>1));
                $ins = $this->db_model->update_Data_limit($this->input->post('table',TRUE),array('status'=>$this->input->post('status',TRUE)),array('id'=>$this->input->post('id',TRUE)));
                if($ins){
                    $resp = array('status'=>1,'msg'=>'Succesfully Change Status');
                }else{
                    $resp = array('status'=>0,'msg'=>'Succesfully Change Status');
                }
                echo json_encode($resp,JSON_UNESCAPED_SLASHES);
            }
        }else{
            echo 'Status Not Change';
        } 
    }
      function deleteData(){
        if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')){
            if(!empty($this->input->post('id',TRUE))){
                $res = $this->db_model->delete_data($this->input->post('table',TRUE),array('id'=>$this->input->post('id',TRUE)));
                $resp = array('status'=>'1', 'msg' =>'Successfully Delete');
            }else{
                $resp = array('status'=>'0','msg' =>'Not Delete'); 
            }
            echo json_encode($resp,JSON_UNESCAPED_SLASHES);
            
        }else{
            echo "Not Delete";
        } 
    }
}
