<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Countdown Timer</title>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/icons8-countdown-48.png" />
	
  <!-- <link rel="stylesheet" href="css/style.css"> -->
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Inspiration&family=Poppins:wght@300;400;500;700&display=swap');

    *{
    margin: 0;
    padding:0;
    font-family: 'Poppins', sans-serif;
    }


    body{
      width: 100vw;
      height: 100vh;
      background: linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)),url('<?php echo base_url('assets/img/img.jpg');?>');
      background-size: cover;
      background-position: center;
      color:#fff;
      display: flex;
      justify-content: center;
      align-items: center;
      flex-direction: column;
    }
    p{
      font-size: 13px;
    }

    .newyear{
      font-family: 'Inspiration', cursive;
      font-size: 50px;
      font-weight: 400;
      margin-top: 30px;
    }

    .counter{
      display: flex;
      margin-top: 30px;
    }

    .box{
      width: 50px;
      height: 50px;
      text-align: center;
    }

    .box h2{
      font-size: 20px;
      font-weight: 500;
    }
    .box small{
      font-size: 10px;
    }
    @media (min-width:576px) {
      p{
        font-size: 18px;
      }
      .newyear{
        font-size: 100px;
      }
      .box{
        width: 100px;
        height: 70px;
      }

      .box h2{
        font-size: 30px;
      }
      .box small{
        font-size:15px;
      }
    }

    @media (min-width:768px) {
      p{
        font-size: 25px;
      }
      .newyear{
        font-size: 150px;
      }
      
      .box{
        width: 150px;
        height: 100px;
      }

      .box h2{
        font-size: 50px;
      }
      .box small{
        font-size:20px;
      }
    }
  </style>
</head>
<body>
  <p>Countdown to</p>
  <h2 class="newyear">New Year</h2>
  <div class="counter">

    <div class="box">
      <h2 id="days">00</h2>
      <small>Days</small>
    </div>

    <div class="box">
        <h2 id="hours">00</h2>
        <small>Hours</small>
      </div>

      <div class="box">
        <h2 id="minutes">00</h2>
        <small>Minutes</small>
      </div>

      <div class="box">
        <h2 id="seconds">00</h2>
        <small>Seconds</small>
      </div>   

  </div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js" integrity="sha512-pumBsjNRGGqkPzKHndZMaAG+bir374sORyzM3uulLV14lN5LyykqNk8eEeUlUkB3U0M4FApyaHraT65ihJhDpQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script>
	
	let stDate = "<?php echo isset($startdata)&&!empty($startdata)?$startdata:'';?>";
	let vryfy = "<?php echo isset($d[0]['veryfy_id'])&&!empty($d[0]['veryfy_id'])?$d[0]['veryfy_id']:'';?>";
	let base_url = "<?php echo base_url();?>";
	const days=document.querySelector("#days");
	const hours=document.querySelector("#hours");
	const minutes=document.querySelector("#minutes");
	const seconds=document.querySelector("#seconds");

	const currentYear=new Date().getFullYear();   
	const newYear=new Date('<?php echo  isset($featuredate)&&!empty($featuredate)?$featuredate:'';?>');	
	$.ajax({
		method: "POST",
		url: base_url + "Welcome/check",
		data:{vryfy:vryfy},
		success: function(resp) {
	        var data = JSON.parse(resp);
			const currentDate=new Date();
			console.log(newYear);
			const newYear1=new Date(data.countdown_time);
			if(data.status==0){
			    return false;
			}else{
    			if(data.checkTime<currentDate){
    				update();
    			}else{				
    				setInterval(function(){
    					const currentDate=new Date();
    					const diff=newYear1-currentDate;
    					if(diff >= "0"){
    							function UpdateTime(){
    							const currentDate=new Date();
    							const diff=newYear-currentDate;			
    							const d=Math.floor(diff/1000/60/60/24);
    							const h=Math.floor((diff/1000/60/60)%24);
    							const m=Math.floor((diff/1000/60)%60);
    							const s=Math.floor((diff/1000)%60);
    							const ms=Math.floor(diff%1000);
    
    							days.innerHTML=d<10?"0"+d:d;
    							hours.innerHTML=h<10?"0"+h:h;
    							minutes.innerHTML=m<10?"0"+m:m;
    							seconds.innerHTML=s<10?"0"+s:s;
    							// mseconds.innerHTML=ms<10?"0"+ms:ms;
    						}
    						setInterval(UpdateTime,1000);
    					}else{
    						clearInterval();	
    						update();					
    					}
    				});	
    			}
			}
		}
	});
		function update(){
			var i = 1;
			if(i==1){
				 xhrPool = []; 
				 $.ajax({
				     method: "POST",
				     url: base_url + "Welcome/updateCountDown",
				     success: function(jqXHR,resp) {
			             var data = JSON.parse(resp);
			             if(data.status==1){
			                   console.log('already update countdown');
			                   location.reload(); 
			             }else{
    				         xhrPool.push(jqXHR); 
    				         window.location.reload();clearInterval();
			             }
				     }
				     
				 });
				 return "update Time";
				i++;
			}else{
				clearInterval();
				return window.location.reload();;
				xhrPool.push(jqXHR); 
			}
		}
  </script>
</body>
</html>
