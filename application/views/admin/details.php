
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
	 <title>Add Timer</title>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/toastr.min.css';?>"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/icons8-countdown-48.png" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datatable/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="  https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css" integrity="sha384-QYIZto+st3yW+o8+5OHfT6S482Zsvz2WfOzpFSXMF9zqeLcFV0/wlZpMtyFcZALm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	
	<script>
	    	let base_url = "<?php echo base_url();?>";
	</script>
</head>

<body>
	<div class="">
	    <div class="container">
          <div class="row py-5">
            	<div class="field_div ">
    			    <a href="<?php echo base_url();?>add-timer" class="btn btn-primary btn-xs dt-edit">Add New </a>
    			</div>
				<!--<div class="field_div ">-->
    <!--			   <a href="<?php echo base_url();?>" target="_blank">Check Live Countdown </a>-->
    <!--			</div>-->
            <div class="col-12">
                <div class="edu_main_wrapper edu_table_wrapper">		
        			<div class="edu_admin_informationdiv sectionHolder dropdown_height">
                        <div class="tableFullWrapper">
                            <table class="server_datatable table table-striped table-bordered" cellspacing="0" width="100%" data-url="welcome/details">
                                <thead>
                                   <tr>
                                    <th>Project Name</th>
                                    <th>Start Date & Time</th>
                                    <th>Initial Value</th>
                                    <th>Reset Time</th>
                                    <th>Countdown Over</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                  </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
        			</div>
        		</div>
            </div>
          </div>
        </div>
	</div>

<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script>


</body>

</html>
