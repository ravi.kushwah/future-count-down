<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
	<link rel="stylesheet" href="<?php echo base_url('assets/css/main.css'); ?>">
	 <title>Add Timer</title>
	 <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/toastr.min.css';?>"/>
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>assets/img/icons8-countdown-48.png" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datatable/dataTables.bootstrap.css">
    <link rel="stylesheet" type="text/css" href="  https://cdn.datatables.net/responsive/2.2.9/css/responsive.bootstrap4.min.css"/>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.2.1/css/fontawesome.min.css" integrity="sha384-QYIZto+st3yW+o8+5OHfT6S482Zsvz2WfOzpFSXMF9zqeLcFV0/wlZpMtyFcZALm" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css" integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<script>
	    	let base_url = "<?php echo base_url();?>";
	</script>
</head>

<body>
  <?php 
    $rt= isset($c_deatils[0]['reset_type'])?$c_deatils[0]['reset_type']:'';
    $it= isset($c_deatils[0]['initial_timer_type'])?$c_deatils[0]['initial_timer_type']:'';
  
  if($rt=='minute'){
     $m = 'checked';
  }  else if($rt=='hours'){
    $h = 'checked';
  }else{
      $e="";
  }
  if($it=='minute'){
     $mm = 'checked';
  }  else if($it=='hours'){
    $hh = 'checked';
  }else{
      $ee="";
  }
 
  ?>
	<div class="bg-img addtimer">
		<div class="content">
			<header>Add Count Down Timer</header>
			<form class="form" method="post">
				<div class="row">
					<div class="col-lg-12">
						<div class="field_div">
							<label class="form-label">Project Name</label>
							<input type="text" class="form-control" id="project_name" name="project_name" value="<?php echo isset($c_deatils[0]['project_name'])?$c_deatils[0]['project_name']:'';?>" placeholder="Project Name">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="field_div">
							<label class="form-label">Start Date & Time</label>
							<input type="datetime-local" class="form-control" value="<?php echo isset($c_deatils[0]['start_date'])?$c_deatils[0]['start_date']:'';?>" id="date-time" name="date" placeholder="Start Date & Time">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="field_div">
							<label class="form-label">Initial Timer	</label>
							<span></span>
						<div class="field_checkbox">
						 <div class="field_checkbox1">
						     <label class="form-label">Hours</label>
						      <input type="checkbox" class="int_hour" name="hours" <?php echo isset($hh)?$hh:'';?> <?php echo isset($ee)?'checked':'';?> value="<?php if(isset($c_deatils[0]['initial_timer_type'])=='hours'){echo $c_deatils[0]['initial_timer_type'];}else{echo "";}?>" id="hours" >
						 </div>
						<div class="field_checkbox1">
						     <label class="form-label">Minute</label>
						    <input type="checkbox" class="int_time" name="minute" <?php echo isset($mm)?$mm:isset($ee);?> value="<?php if(isset($c_deatils[0]['initial_timer_type'])){echo $c_deatils[0]['initial_timer_type'];}else{echo "";}?>" id="minute">
						</div>
						</div>
						    <input type="text" class="form-control minute_hours" value="<?php echo isset($c_deatils[0]['initial_value'])?$c_deatils[0]['initial_value']:'';?>" id="minute_hours" value=""  minlength="1" maxlength="2" placeholder="Initial Hours">
					        <input type="text" class="form-control minute_value" value="<?php echo isset($c_deatils[0]['initial_value'])?$c_deatils[0]['initial_value']:'';?>" id="minute_value" value="" minlength="1" maxlength="2" placeholder="Initial Minute">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="field_div">
							<label class="form-label">Reset After Every</label>
							<span></span>
						<div class="field_checkbox">
						 <div class="field_checkbox1">
						     <label class="form-label">Hours</label>
						      <input type="checkbox" class="int_hour" <?php echo isset($h)?$h:'';?> <?php echo isset($e)?'checked':'';?> name="hours"value="<?php echo isset($c_deatils[0]['reset_type'])?$c_deatils[0]['reset_type']:'';?>"  id="resthours">
						 </div>
						<div class="field_checkbox1">
						     <label class="form-label">Minute</label>
						    <input type="checkbox" class="int_time" <?php echo isset($m)?$m:'';?>  name="minute"value="<?php echo isset($c_deatils[0]['reset_type'])?$c_deatils[0]['reset_type']:'';?>"  id="restminute">
						</div>
						</div>
						    <input type="text" class="form-control rest_hours "value="<?php echo isset($c_deatils[0]['reset_time'])?$c_deatils[0]['reset_time']:'';?>" id="rest_hours" value=""  minlength="1" maxlength="2" placeholder="Reset Hours">
					        <input type="text" class="form-control rest_times"value="<?php echo isset($c_deatils[0]['reset_time'])?$c_deatils[0]['reset_time']:'';?>" id="rest_times" value="" minlength="1" maxlength="2" placeholder="Reset Minute">
						</div>
					</div>
					<div class="col-lg-12">
						
					</div>
				</div>
			</form>
			<div class="divide_flex">
			    <div class="field_div">
					<button class="<?php echo isset($c_deatils[0]['id'])?'UpdateCountdown':'addCountdown';?>" data-id="<?php echo isset($c_deatils[0]['id'])?$c_deatils[0]['id']:'';?>"><?php echo isset($c_deatils[0]['id'])?'Update Count Down':'Add Count Down';?></dubbton>
				</div>
				
				<div class="field_div countdownDetails">
    				<button class="DetailsTimer"> <a href="<?php echo base_url();?>details-countdown">Timer Details</a></dubbton>
    			</div>
			</div>
				
		</div> 
	</div>

<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/toastr.min.js"></script>
<script src="<?php echo base_url();?>assets/js/datatable/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/js/main.js"></script>
</body>

</html>
