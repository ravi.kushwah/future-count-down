$(document).ready(function () {
    toastr.clear();
    toastr.options = {
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "fadeIn": 300,
            "fadeOut": 1000,
            "timeOut": 5000,
            "extendedTimeOut": 1000
        }
 
    if (typeof MathJax !== 'undefined') { MathJax.Hub.Queue(["Typeset", MathJax.Hub]); }
    var dataTableObj = $('.server_datatable').DataTable({
        searching: true,
        processing: true,
        dom: 'lf<"table-responsive" t >ip',
        language: {
            paginate: {
                previous: '<i class="fa fa-chevron-left"></i>',
                next: '<i class="fa fa-chevron-right"></i>',
            },
            emptyTable:  "Data Not Found",
            search:   'Search:',

        },
        pageLength: 10,
        lengthMenu: [
            [10, 25, 50, 100, -1],
            [10, 25, 50, 100, "All"]
        ],
        responsive: true,
        serverSide: { "regex": true },
        columnDefs: [{
            targets: "_all",
            orderable: false
        }],
        ajax: {
            "url": base_url + $('.server_datatable').attr('data-url'),
            "type": "POST"
        }
    });
    // $('.timerDetailsData').hide();
    // $(document).on('click','.DetailsTimer',function(e){
    //      e.preventDefault();
    //   $('.timerDetailsData').show(500);
    //   $('.addtimer').hide();
    // });	
    $(document).on('click', '.changeStatusButton', function() {
        var table = $(this).attr('data-table');
        var status = $(this).attr('data-status');
        var data_tabel = $(this).attr('data-table');
        $.ajax({
            method: "POST",
            url: base_url + "welcome/change_status",
            data: { 'id': $(this).attr('data-id'), 'table': $(this).attr('data-table'), 'status': $(this).attr('data-status') },
            success: function(resp) {
                var resp = $.parseJSON(resp);
                if (resp['status'] == '1') {
                    toastr.success('Successfully Change Status');
                    targetTableUrl = $('.server_datatable').attr('data-url');
                    dataTableObj.ajax.url(base_url + targetTableUrl).load();

                } else {
                    toastr.error('Technical issue');
                }
            },
            error: function(resp) {
                toastr.error('Technical issue');
            }
        });
    });	
     $(document).on('click', '.deleteData', function() {
         $.ajax({
            method: "POST",
            url: base_url + "welcome/deleteData",
            data: { 'id': $(this).attr('data-id'), 'table': $(this).attr('data-table')},
            success: function(resp) {
                var resp = $.parseJSON(resp);
                if (resp['status'] == '1') {
                    toastr.success(resp['msg']);
                    targetTableUrl = $('.server_datatable').attr('data-url');
                    dataTableObj.ajax.url(base_url + targetTableUrl).load();
    
                } else {
                   toastr.error('Technical issue');
                }
            },
            error: function(resp) {
               toastr.error('Technical issue');
            }
        });
     });
     $(document).on('click','.addCountdown',function(e){
        e.preventDefault();
        var datetime=$('#date-time').val();
	    var initialvalue=$('#initial-value').val();
	    var resetvalue=$('#reset-value').val()
	    var project_name=$('#project_name').val()
	    
	    var minute_hours =$('#minute_hours').val()
	    var minute_value =$('#minute_value').val()
	    var rest_hours =$('#rest_hours').val()
	    var rest_times =$('#rest_times').val()
	  
	    var hours =$('#hours').is(":checked")
	    var minute =$('#minute').is(":checked")
	    var resthours =$('#resthours').is(":checked")
	    var restminute =$('#restminute').is(":checked")
       
        $.ajax({
            method: "POST",
            url: base_url + "add-countdown-timer",
            data: {
                datetime:datetime,
                initialvalue:initialvalue,
                resetvalue:resetvalue,
                project_name:project_name,
                minute_hours :minute_hours,
                minute_value :minute_value,
                rest_hours :rest_hours,
                rest_times :rest_times,
                hours:hours,
                minute:minute,
                resthours:resthours,
                restminute:restminute,
            },
            success: function(resp) {
              var resp = $.parseJSON(resp);
              if(resp.status==1){
                toastr.success(resp.res);
                setTimeout(function(){window.location.reload();},1500);
              }else{
                toastr.success(resp.res);
              }
               
            },
            error: function(resp) {
              console.log(resp.data.length==0);
            }
        });
    });	
     $(document).on('click','.UpdateCountdown',function(e){
        e.preventDefault();
        var datetime=$('#date-time').val();
        var id=$(this).attr('data-id');
	    var initialvalue=$('#initial-value').val();
	    var resetvalue=$('#reset-value').val()
	    var project_name=$('#project_name').val()
	    
	    var minute_hours =$('#minute_hours').val()
	    var minute_value =$('#minute_value').val()
	    var rest_hours =$('#rest_hours').val()
	    var rest_times =$('#rest_times').val()
	  
	    var hours =$('#hours').is(":checked")
	    var minute =$('#minute').is(":checked")
	    var resthours =$('#resthours').is(":checked")
	    var restminute =$('#restminute').is(":checked")
       
        $.ajax({
            method: "POST",
            url: base_url + "edit-timer",
            data: {
                datetime:datetime,
                initialvalue:initialvalue,
                resetvalue:resetvalue,
                project_name:project_name,
                minute_hours :minute_hours,
                minute_value :minute_value,
                rest_hours :rest_hours,
                rest_times :rest_times,
                hours:hours,
                minute:minute,
                resthours:resthours,
                restminute:restminute,
                id:id,
            },
            success: function(resp) {
              var resp = $.parseJSON(resp);
              if(resp.status==1){
                toastr.success(resp.res);
                setTimeout(function(){window.location.reload();},1500);
              }else{
                toastr.success(resp.res);
              }
               
            },
            error: function(resp) {
              console.log(resp.data.length==0);
            }
        });
    });	
    
        $(".minute_value").hide();
        $(document).on('click','#hours',function(){
            var chk = $(this).val();
             if($(this).is(":checked")) {
                $(".minute_hours").show(500);
                $(".minute_value").hide(500);
               $('#minute_hours').prop('checked', true);
               $('#minute').prop('checked', false);
               }
        });
        $(document).on('click','#minute',function(){
            var chk = $(this).val();
             if($(this).is(":checked")) {
                $(".minute_value").show(500);
                $(".minute_hours").hide(500);
                $('#minute').prop('checked', true);
                $('#hours').prop('checked', false);
         }
    });
    
    
     $(".rest_times").hide();
        $(document).on('click','#resthours',function(){
            var chk = $(this).val();
             if($(this).is(":checked")) {
                $(".rest_hours").show(500);
                $(".rest_times").hide(500);
               $('#rest_hours').prop('checked', true);
               $('#restminute').prop('checked', false);
               }
        });
        $(document).on('click','#restminute',function(){
            var chk = $(this).val();
             if($(this).is(":checked")) {
                $(".rest_times").show(500);
                $(".rest_hours").hide(500);
                $('#restminute').prop('checked', true);
                $('#resthours').prop('checked', false);
         }
    });
    $(document).on('click','.editTImer',function(){
        var id = $(this).attr('data-id');
        window.location.href= base_url + "edit-time/"+id;
    });
     $(document).on('click','.copyButton',function(){
        var id = $(this).attr('data-get-id');
        var temp = $("<input>");
        $("body").append(temp);
        temp.val($('#'+id).text()).select();
        document.execCommand("copy");
        temp.remove();
        console.timeEnd('time1');
        toastr.success('Successfully Copy Js');
    });
    
});
    function withoutJquery(){
       console.time('time2');
    	var temp=document.createElement('input');
      var texttoCopy=document.getElementById('copyText2').innerHTML;
      temp.type='input';
      temp.setAttribute('value',texttoCopy);
      document.body.appendChild(temp);
        temp.select();
      document.execCommand("copy");
      temp.remove();
       console.timeEnd('time2');
       toastr.success('Successfully Copy HTML');
    }
